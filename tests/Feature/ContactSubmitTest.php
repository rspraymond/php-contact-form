<?php

namespace Tests\Feature;

use App\ContactSubmission;
use Tests\TestCase;

class ContactSubmitTest extends TestCase
{
    public function testContactSubmit()
    {
        $factory = factory(ContactSubmission::class)->make();

        $this->post('/contact', $factory->toArray());

        $submission = ContactSubmission::orderBy('id', 'DESC')->first();

        $this->assertEquals($factory->name, $submission->name);
        $this->assertEquals($factory->email, $submission->email);
        $this->assertEquals($factory->phone, $submission->phone);
        $this->assertEquals($factory->message, $submission->message);
    }
}
