<?php

namespace Tests\Feature;

use App\ContactSubmission;
use App\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ContactSubmitApiTest extends TestCase
{
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->createAndAuthUser();
    }

    public function testCreateSubmission()
    {
        $payload = [
            'name' => 'John Doe',
            'email' => 'jon@example.org',
            'message' => 'I had a good day.'
        ];

        $this->json('POST', '/api/contact-submissions/create', $payload)
            ->assertStatus(201)
            ->assertJson($payload);
    }

    public function testListSubmissions()
    {
        $this->json('GET', '/api/contact-submissions')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => ['id', 'name', 'email', 'phone', 'message', 'created_at', 'updated_at']
                ]
            ]);
    }

    public function testShowSubmission()
    {
        $submission = factory(ContactSubmission::class)->create();

        $this->json('GET', '/api/contact-submissions/' . $submission->id)
            ->assertStatus(200)
            ->assertJson($submission->toArray());
    }

    public function testDeleteSubmission()
    {
        $submission = factory(ContactSubmission::class)->create();

        $this->json('DELETE', '/api/contact-submissions/' . $submission->id)
            ->assertStatus(204);
    }

    /**
     * Create User and Auth for testing.
     */
    protected function createAndAuthUser()
    {
        $this->user = factory(User::class)->create();

        Passport::actingAs(
            $this->user
        );
    }
}
