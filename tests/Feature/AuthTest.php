<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Lang;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    protected $defaultPassword = 'Boogity!@#';

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = factory(User::class)->create(['password' => bcrypt($this->defaultPassword)]);

        $this->post('/login', ['email' => $user->email, 'password' => $this->defaultPassword])
            ->assertRedirect('/home');
    }

    public function testAuthRedirect()
    {
        $user = factory(User::class)->make();

        $this->actingAs($user)->get('/login')->assertRedirect('/home');
    }

    public function testRegister()
    {
        $user = factory(User::class)->make();
        $password_data = ['password' => $this->defaultPassword, 'password_confirmation' => $this->defaultPassword];

        $this->post('/register', array_merge($user->toArray(), $password_data))->assertRedirect('/home');
    }

    public function testForgotPassword()
    {
        $user = factory(User::class)->create();
        $password_language = Lang::get('passwords');

        $this->post('password/email', ['email' => $user->email])->assertSessionHas('status', $password_language['sent']);
    }
}
