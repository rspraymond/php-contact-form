<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('contact-submissions', 'ContactController@index');
    Route::get('contact-submissions/{submission}', 'ContactController@show');
    Route::post('contact-submissions/create', 'ContactController@store');
    Route::delete('contact-submissions/{submission}', 'ContactController@delete');
});

Route::fallback(function () {
    return response()->json(['message' => __('messages.not_found')], 404);
});
