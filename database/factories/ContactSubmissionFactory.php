<?php

use App\ContactSubmission;
use Faker\Generator as Faker;

$factory->define(ContactSubmission::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => str_replace('+', '', $faker->phoneNumber),
        'message' => $faker->paragraph
    ];
});
