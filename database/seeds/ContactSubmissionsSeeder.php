<?php

use App\ContactSubmission;
use Illuminate\Database\Seeder;

class ContactSubmissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactSubmission::truncate();

        factory(ContactSubmission::class, 50)->create();
    }
}
