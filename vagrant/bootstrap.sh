#!/usr/bin/env bash

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install git
sudo apt-get install zip

sudo apt-get -y install debconf

# MySql
sudo echo mysql-server-5.7 mysql-server/root_password password root | sudo debconf-set-selections
sudo echo mysql-server-5.7 mysql-server/root_password_again password root | sudo debconf-set-selections
sudo apt-get -y install mysql-server
sudo apt-get -y install zip unzip

# PHP
sudo apt-get install -y libapache2-mod-php7.0
sudo apt-get install -y php7.0-mysql
sudo apt-get install -y php7.0-curl
sudo apt-get install -y php7.0-json
sudo a2enmod php7.0
sudo apt-get install -y php7.0-mbstring
sudo apt-get install -y php7.0-xml
sudo apt-get install -y php7.0-zip
sudo apt-get install -y php-xdebug
sudo apt-get install php7.0-sqlite3

# NPM
sudo apt-get install -y nodejs
sudo apt-get install -y npm
ln -s /usr/bin/nodejs /usr/bin/node

# Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

sudo cp /vagrant/vagrant/000-default.conf /etc/apache2/sites-available/000-default.conf
sudo cp /vagrant/vagrant/apache2.conf /etc/apache2/apache2.conf

sudo a2enmod rewrite
sudo service apache2 restart

echo "cd /vagrant" >> /home/ubuntu/.bashrc