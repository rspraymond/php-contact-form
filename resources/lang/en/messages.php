<?php

return [
    'contact_success' => 'Thanks for contacting us! We will respond ASAP.',
    'not_found' => 'Sorry, the page you are looking for could not be found.',
];
