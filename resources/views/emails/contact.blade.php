@component('mail::message')
# Contact Email

** Date: ** {{ $submission->created_at->format('Y-m-d') }}<br>
** Name: ** {{ $submission->name }}<br>
** Email: ** {{ $submission->email }}<br>
** Phone: ** {{ $submission->phone }}

** Message: ** {{ $submission->message }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
