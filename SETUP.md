# Project Setup

This project comes bundled with vagrant configuration to help streamline testing. The vagrant setup has been been bootstrapped with required dependancies for running the application.

## Prerequisites
* [Oracle Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

## Terminal Commands
Once you have installed the dependancies you should be able to run the following commands in the root of the repository.

```
vagrant up
vagrant ssh
cd scripts
./local_build.sh
cd ..
# Run Tests
./vendor/bin/phpunit
```

If everything runs correctly. Your application should now be viewable at [http://192.168.50.9](http://192.168.50.9/).

## API Testing
An OAuth2 Restful JSON api with bearer authorization has been setup for this application. The following routes should be testable using a rest client like [postman](https://www.getpostman.com/). You will need to register a user account and personal token to gain access [http://192.168.50.9/register](http://192.168.50.9/register).

* http://192.168.50.9/api/contact-submissions (GET) - Lists submissions.
* http://192.168.50.9/api/contact-submissions/{submission_id} (GET) - Lists individual submissions.
* http://192.168.50.9/api/contact-submissions/add (POST) - Adds a submission.
* http://192.168.50.9/api/contact-submissions/{submission_id} (DELETE) - Deletes a submission.