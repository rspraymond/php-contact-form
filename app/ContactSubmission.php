<?php

namespace App;

use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;

class ContactSubmission extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'message'];

    /**
     * Send out notification for this submission.
     * @return $this
     */
    public function send()
    {
        $this->save();
        $contact_mail_to = env('CONTACT_EMAIL', 'guy-smiley@example.com');
        Mail::to($contact_mail_to)->send(new ContactEmail($this));

        return $this;
    }
}
