<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Route method for showing user dashboard.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        return view('home');
    }

    /**
     * Route method for showing home page.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }
}
