<?php

namespace App\Http\Controllers;

use App\ContactSubmission;
use App\Http\Requests\ContactSubmitRequest;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class ContactController extends Controller
{
    /**
     * Route method for the purpose of processing a contact submit.l
     * @param ContactSubmitRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function contactSubmit(ContactSubmitRequest $request)
    {
        $submission = new ContactSubmission($request->input());
        $submission->send();
        $request->session()->flash('success_message', __('messages.contact_success'));
        return redirect(URL::previous());
    }

    /**
     * API route method for getting all contact submissions.
     * @return mixed
     */
    public function index()
    {
        return ContactSubmission::Paginate();
    }

    /***
     * API route method for getting a contact submission.
     * @param ContactSubmission $submission
     * @return ContactSubmission
     */
    public function show(ContactSubmission $submission)
    {
        return $submission;
    }

    /***
     * API route method for saving a new contact submission.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ContactSubmitRequest $request)
    {
        $submission = ContactSubmission::create($request->all());
        $submission->send();

        return response()->json($submission, 201);
    }


    /**
     * API route method for deleting a contact submission.
     * @param Request $request
     * @param ContactSubmission $submission
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Request $request, ContactSubmission $submission)
    {
        $submission->delete();

        return response()->json(null, 204);
    }
}
