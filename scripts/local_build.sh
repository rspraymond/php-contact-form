#!/usr/bin/env bash

cd ..
# Copy .env.example file if .env does not exist.
cp -n ./.env.example ./.env
sudo npm cache clean -f
sudo npm install -g n
sudo n latest
sudo npm install
sudo npm rebuild node-sass --force
composer install
echo "CREATE DATABASE IF NOT EXISTS laravel_app" | mysql -uroot -proot
php artisan key:generate
php artisan migrate
php artisan passport:install
sudo npm run dev
