#!/usr/bin/env bash

sudo cp /vagrant/vagrant/000-default.conf /etc/apache2/sites-available/000-default.conf
sudo cp /vagrant/vagrant/apache2.conf /etc/apache2/apache2.conf

sudo service apache2 restart
sudo service mysql restart